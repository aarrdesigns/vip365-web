<?php

namespace App\Models\Access\Setting;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{

    protected $table ="settings";

    protected $fillable = [
        'distance_rate', 'minute_rate', 'minimun_rate', 'base_rate'
    ];

    protected $hidden = [
        'id', 'created_at', 'updated_at'
    ];
}
