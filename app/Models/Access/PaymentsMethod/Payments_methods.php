<?php

namespace App\Models\Access\PaymentsMethod;

use Illuminate\Database\Eloquent\Model;

class Payments_methods extends Model
{
    protected $table ="payments_methods";

    protected $fillable = [
        'id', 'user_id', 'payment_type', 'isDefault', 'isActive'
    ];

    protected $hidden = ['card_type', 'card_number', 'card_security_number', 'card_holder_name', 'paypal_code'];

    public function user() {
        return $this->belongsTo('App\Models\Access\Users\User');
    }
}
