<?php

namespace App\Models\Access\Clients;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Client
 * @package App\Models\Access\Clients
 */
class Client extends Model {

    protected $table ="clients";

    protected $fillable = ['id', 'user_id', 'mount_debt', 'trips_count'];


//    AuthApiController

    /**
     * An Client is owned by a user
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user() {
        return $this->belongsTo(User::class);
    }

}

