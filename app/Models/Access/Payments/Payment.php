<?php

namespace App\Models\Access\PaymentsMethod;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table ="payments";
    protected $fillable = [
        'id', 'user_id', 'trip_id', 'payment_method_id', 'amount', 'isApproved'
    ];

    public function user() {
        return $this->belongsTo('App\Models\Access\Users\User');
    }

    public function trip() {
        return $this->belongsTo('App\Models\Access\Trips\Trip');
    }

    public function payment_method() {
        return $this->belongsTo('App\Models\Access\PaymentsMethod\Payments_methods');
    }

}
