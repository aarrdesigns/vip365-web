<?php

namespace App\Models\Access\Drivers;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{

    protected $table ="drivers";

    protected $fillable = [
        'id', 'user_id', 'car_id', 'driver_past_location', 'driver_new_location', 'driver_state'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function car() {
        return $this->belongsTo(Car::class);
    }
}
