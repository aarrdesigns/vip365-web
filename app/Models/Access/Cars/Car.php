<?php

namespace App\Models\Access\Drivers;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{

    protected $table ="cars";

    protected $fillable = [
        'id', 'car_color', 'car_plaque', 'car_model', 'car_brand'
    ];
}
