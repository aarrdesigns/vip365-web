<?php

namespace App\Models\Access\Users;

use App\Models\Access\Clients\Client;
use App\Models\Access\Drivers\Driver;
use App\Models\Access\Role\Role;
use Illuminate\Notifications\Notifiable;
use App\Models\Access\Users\Traits\UserAccess;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Access\Users\Traits\Scope\UserScope;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\Access\Users\Traits\UserSendPasswordReset;
use App\Models\Access\Users\Traits\Attribute\UserAttribute;
use App\Models\Access\Users\Traits\Relationship\UserRelationship;

/**
 * Class User.
 */
class User extends Authenticatable
{
    use UserScope,
        UserAccess,
        Notifiable,
        SoftDeletes,
        UserAttribute,
        UserRelationship,
        UserSendPasswordReset;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table;

    /**
     * @var string
     */
    public $primaryKey  = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'user_type', 'type', 'first_name', 'last_name', 'email', 'country_code', 'phone_number', 'password', 'status', 'confirmation_code', 'confirmed'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * @var array
     */
    protected $dates = [ 'deleted_at'];

    /**
     * The dynamic attributes from mutators that should be returned with the user object.
     * @var array
     */
//    protected $appends = ['full_name', 'name'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        $this->table = config('access.users_table');
    }
//    AuthApiController

    /**
     * A user have many clients
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function client() {
        return $this->hasOne(Client::class,'user_id','id');
    }
    public function driver() {
        return $this->hasOne(Driver::class,'user_id','id');
    }

//    public function roles()
//    {
//        return $this->belongsTo(Role::class);
//    }
}
