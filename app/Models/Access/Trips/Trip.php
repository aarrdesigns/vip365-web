<?php

namespace App\Models\Access\Trips;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $table ="trips";

    protected $fillable = [
        'id', 'user_client_id', 'user_drive_id', 'request_date', 'trip_date', 'origin_location', 'destiny_location', 'origin_name', 'destiny_name',
        'state', 'path_taked', 'trip_cost', 'trip_type', 'payment_id', 'isPaid', 'client_clasification', 'driver_clasification'
    ];

    protected $hidden = ['created_at', 'updated_at'];

    public function client() {
        return $this->belongsTo('App\Models\Access\Clients\Client');
    }

    public function driver() {
        return $this->belongsTo('App\Models\Access\Drivers\Driver');
    }
}
