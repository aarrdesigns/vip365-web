<?php

namespace App\Http\Controllers\ApiController;

use App\Models\Access\Drivers\Driver;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Access\Drivers\Setting;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class DriversApiController extends Controller {

    public function index() {
       $drivers = Setting::all();
       return View::make('drivers.index')->with('drivers', $drivers);
    }

    public function create() {
        return View::make('drivers.create');
    }

    public function store(Request $request) {
       return 'create';
    }

    public function getDriver(Request $request, $id) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required'

            ]
        );

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return [
                'Drive' => null,
                'isError'=> true,
                'msgError' => $error
            ];
        }

        else {
            $id = $request->input('id');
            $drive =  Driver::find($id);
            return [
               'Drive' => [
                    $drive,
                ],
                'isError'=> false,
                'msgError' => ''
            ];
        }
    }

    public function edit($id) {
    }

    public function getNearDriver() {
        $validator = Validator::make(Input::all(),
            [
                'current_location' => 'required'
            ]
        );

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return ['id'=> -1, 'isError'=> true, 'msgError' => ''];
        }

        else {
            $driver = Driver::all();

//
            $current_location = Input::get('current_location');
            $distance = explode(',', $current_location);
            $latitud = $distance[0];
            $longitud = $distance[1];


//            $driver->id = Input::get('id');
//            $driver->driver_past_location = Input::get('driver_past_location');
//            $driver->driver_new_location = Input::get('driver_new_location');
//            $driver->save();
            dd ($distance);
//            return ['id'=> $driver['id'], 'isError'=> false, 'msgError' => ''];
        }
    }

//    Funcion para calcular kilometros

    public function distanceBetweenTwoCoordinates(
        $latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371000)
    {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        return $angle * $earthRadius;
    }

    public function updateDriver($id) {
        $validator = Validator::make(Input::all(),
            [
                'id' => 'required',
                'current_location' => 'required'

            ]
        );

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return ['id'=> -1, 'isError'=> true, 'msgError' => ''];
        }

        else {
//            $myArray = explode(',', $current_location);
            $current_location = Input::get('current_location');
            $driver = Driver::find($id);
            $driver_new_location = $driver['driver_new_location'];
            $driver->driver_past_location = $driver_new_location;
            $driver->driver_new_location = $current_location;
            $driver->save();
            return [
                'Drivers'=> [
                    'driver_past_location' => $driver['driver_past_location'],
                    'driver_new_location' => $driver['driver_new_location'],
                ],
                'isError'=> false,
                'msgError' => ''];
        }
    }

//    DELETE example.com/drivers/{id}
    public function destroy($id) {
//        $driver = Driver::find($id);
//        $driver->delete();
//
//        // redirect
//        Session::flash('message', 'Successfully deleted the driver!');
//        return Redirect::to('drivers');
    }
}
