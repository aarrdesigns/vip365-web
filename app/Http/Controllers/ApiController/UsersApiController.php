<?php

namespace App\Http\Controllers\ApiController;

use App\Models\Access\Users\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UsersApiController extends Controller
{
    public function index() {
        $users = User::where('type', 'User')
            ->orderBy('id', 'asc')
            ->get();
        return response()->json($users);
    }

    public function show($id) {
        return 'show';
    }

    public function store(Request $request) {
        return 'store';
    }

    public function update(Request $request, $id) {
        return 'updates';
    }

    public function delete(Request $request, $id) {
        return 'delete';
    }
}
