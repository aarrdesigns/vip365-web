<?php

namespace App\Http\Controllers\ApiController;

use App\Models\Access\PaymentsMethod\Payments_methods;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class PaymentsMethodsApiController extends Controller
{
    //    GET     example.com/payments-method
    public function index() {
        $payments_methods = Payments_methods::all();
        return ['all_payments_methods'=> $payments_methods, 'isError'=> true, 'msgError' => ''];
    }

//     GET      example.com/payments-method/create
    public function create() {
        return View::make('payments_methods.create');
    }

//    POST    example.com/payments-method
    public function registerPaymentsMethods(Request $request) {
        $validator = Validator::make($request->all(),
            [
                'user_id' => 'required',
                'card_type' => 'required',
                'card_number' => 'required',
                'card_security_number' => 'required',
                'card_holder_name' => 'required',
                'payment_type' => 'required',
                'isDefault' => 'required',
                'isActive' => 'required'
            ]
        );

        // crear metodo de pago
        $payment_method = new Payments_methods();

        // obtener inputs
        $payment_method->user_id = $request->input('user_id');
        $payment_method->card_type = $request->input('card_type');
        $payment_method->card_number = $request->input('card_number');
        $payment_method->card_security_number = $request->input('card_security_number');
        $payment_method->card_holder_name = $request->input('card_holder_name');
        $payment_method->payment_type = $request->input('payment_type');
        $payment_method->expDate = $request->input('expDate');
        $payment_method->paypal_code= $request->input('paypal_code');
        $payment_method->isDefault = $request->input('isDefault');
        $payment_method->isActive = $request->input('isActive');

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return ['id'=> '', 'isError'=> true, 'msgError' => -1];
        }

        else if (!$validator->fails()) {
            $payment_method->save();
            return ['id'=> $payment_method['id'], 'isError'=> false, 'msgError' => ''];
        }

        else {
            return ['id'=> '', 'isError'=> true, 'msgError' => -1];
        }
    }

    //    Actualizar TRIP
    public function updatePaymentsMethods(Request $request, $id) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required',
                'user_id' => 'required',
                'card_type' => 'required',
                'card_number' => 'required',
                'card_security_number' => 'required',
                'card_holder_name' => 'required',
                'payment_type' => 'required',
                'isDefault' => 'required',
                'isActive' => 'required'
            ]
        );

        $update_payments_methods = Trip::find($id);

        $update_payments_methods->user_id = $request->input('user_id');
        $update_payments_methods->card_type = $request->input('card_type');
        $update_payments_methods->card_number = $request->input('card_number');
        $update_payments_methods->card_security_number = $request->input('card_security_number');
        $update_payments_methods->card_holder_name = $request->input('card_holder_name');
        $update_payments_methods->payment_type = $request->input('payment_type');
        $update_payments_methods->expDate = $request->input('expDate');
        $update_payments_methods->paypal_code= $request->input('paypal_code');
        $update_payments_methods->isDefault = $request->input('isDefault');
        $update_payments_methods->isActive = $request->input('isActive');
        $update_payments_methods->save();

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return ['id'=> -1, 'isError'=> true, 'msgError' => ''];
        }

        else if (!$validator->fails()) {
            $update_payments_methods->save();
            return ['id'=> $update_payments_methods['id'], 'isError'=> false, 'msgError' => ''];
        }

        else {
            return ['id'=> -1, 'isError'=> true, 'msgError' => ''];
        }
    }

    public function getPaymentsMethods(Request $request, $id) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required'

            ]
        );

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return [
                'list'=> null,
                'isError'=> true,
                'msgError' => ''
            ];
        }

        else {
            $id = $request->input('id');
            $payments_methods = Payments_methods::find($id);
            if (($payments_methods != '')){
                return [
                    'list'=> [
                        'id' => $payments_methods['id'],
                        'user_id' => $payments_methods['user_id'],
                        'card_type' => $payments_methods['card_type'],
                        'card_number' => $payments_methods['card_number'],
                        'card_security_number' => $payments_methods['card_security_number'],
                        'card_holder_name' => $payments_methods['card_holder_name'],
                        'expDate' => $payments_methods['expDate'],
                        'payment_type' => $payments_methods['payment_type'],
                        'paypal_code' => $payments_methods['paypal_code'],
                        'isDefault' => $payments_methods['isDefault'],
                        'isActive' => $payments_methods['isActive'],
                    ],
                    'isError'=> false,
                    'msgError' => ''];
            }
            else {
                return 'No hay datos';
            }
        }
    }

//   GET      example.com/payments-method/{id}/edit
    public function edit($id) {
//        $driver = Drive::find($id);
//        return View::make('Payments_methods.edit')
//            ->with('driver', $driver);
    }

////   PUT     http://localhost:8888/vip365-web/api/payments-method/2?id=2&driver_location=amazonas
//    public function updatePaymentsMethods($id) {
//        $validator = Validator::make(Input::all(),
//            [
//                'user_id' => 'required',
//                'card_type' => 'required',
//                'card_number' => 'required',
//                'card_security_number' => 'required',
//                'card_holder_name' => 'required',
//                'payment_type' => 'required',
//                'paypal_code' => 'required',
//                'isDefault' => 'required'
//
//            ]
//        );
//
//        if ($validator->fails()) {
//            $error = $validator->errors()->first();
//            return ['id'=> '', 'isError'=> true, 'msgError' => -1];
//        }
//
//        else {
//            $driver = Payments_methods::find($id);
//            $driver->id = Input::get('id');
//            $driver->driver_location = Input::get('driver_location');
//            $driver->save();
//            return ['id'=> $driver['id'], 'isError'=> false, 'msgError' => ''];
//        }
//    }

//    DELETE example.com/payments-method/{id}

    public function destroy($id) {
//        $Payments_methods= Payments_methods::find($id);
//        $Payments_methods->delete();
//
//        // redirect
//        Session::flash('message', 'Successfully deleted the driver!');
//        return Redirect::to('Payments_methods');
    }
}
