<?php

namespace App\Http\Controllers\ApiController;

use App\Models\Access\Setting\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsApiController extends Controller
{
    public function index() {
        return 'index';
    }

    public function show($id) {
        return 'show';
    }

    public function getSettings() {

        $setting =  Setting::all();
        return [
            'Settings' => $setting,
            'isError'=> false,
            'msgError' => ''
        ];
    }

    public function store(Request $request) {
        return 'store';
    }

    public function update(Request $request, $id) {
        return 'updates';
    }

    public function delete(Request $request, $id) {
        return 'delete';
    }
}