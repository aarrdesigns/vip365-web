<?php

namespace App\Http\Controllers\ApiController;

use App\Models\Access\Clients\Client;
use App\Models\Access\Role\Role;
use App\Models\Access\Users\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;


class AuthApiController extends Controller
{

    public function index() {
        return ('loginApi');
    }

//    Login cliente
    public function loginClient(Request $request) {

        $user_social = $request->input('user_type');

        $validator = Validator::make($request->all(),
            [
                'user_type' => 'required',
            ]
        );

        if (!$validator->fails()) {

            switch ($user_social) {
                // USUARIO NORMAL
                case 1:
                    $validator = Validator::make($request->all(),
                        [
                            'email' => 'required|email',
                            'password' => 'required',
                        ]
                    );

                    if ($validator->fails()) {
                        $error = $validator->errors()->first();
                        return ['Users'=> null, 'isError'=> true, 'msgError' => $error];
                    }
                    else {
                        $credentials['email'] = $request->input('email');
                        $credentials['password'] = $request->input('password');
                        Auth::attempt($credentials);
                        $dataUser = Auth::user();
                        $user_type = $dataUser['user_type'];
                        $roles = $dataUser->roles()->pluck('type');
                        $rol = $roles[0];
                        if ($user_type == 1 && $rol == 2){
                            return ['Users'=> $dataUser, 'isError'=> false, 'msgError' => ''];
                        }
                        else {
                            return 'No es un cliente o esta registrado con red social';
                        }
                    }

                    break;
                case 2:
//                    USUARIO GOOGLE
                    $validator = Validator::make($request->all(),
                        [
                            'email' => 'required|email',
                        ]
                    );

                    if ($validator->fails()) {
                        $error = $validator->errors()->first();
                        return ['Users'=> null, 'isError'=> true, 'msgError' => $error];
                    }
                    else {
//                        Obtener email
                        $credentials['email'] = $request->input('email');
//                        Obtener el usuario con el email que se esta logueando
                        $usuarios = User::where('email','=', $credentials['email'])->first();
//                        obteniendo el id del usuario
                        $id_usuario = $usuarios['id'];
//                        Logueando al usuario mediante el id
                        Auth::loginUsingId($id_usuario);
//                        obteniendo data del usuario logueado.
                        $dataUser = Auth::user();
//                        Obteniendo user_type del usuario
                        $user_type = $dataUser['user_type'];
//                        obteniendo tipo de rol del usuario
                        $roles = $dataUser->roles()->pluck('type');
                        $rol = $roles[0];
                        if ($user_type == 2 && $rol == 2){
                            return ['Users'=> $dataUser, 'isError'=> false, 'msgError' => ''];
                        }
                        else {
                            return ['Users'=> null, 'isError'=> true, 'msgError' => ''];
                        }
                    }
                    break;
                case 3:
//                    USUARIO FACEBOOK
                    $validator = Validator::make($request->all(),
                        [
                            'email' => 'required|email',
                        ]
                    );

                    if ($validator->fails()) {
                        $error = $validator->errors()->first();
                        return ['Users'=> null, 'isError'=> true, 'msgError' => $error];
                    }
                    else {
//                        Obtener email
                        $credentials['email'] = $request->input('email');
//                        Obtener el usuario con el email que se esta logueando
                        $usuarios = User::where('email','=', $credentials['email'])->first();
//                        obteniendo el id del usuario
                        $id_usuario = $usuarios['id'];
//                        Logueando al usuario mediante el id
                        Auth::loginUsingId($id_usuario);
//                        obteniendo data del usuario logueado.
                        $dataUser = Auth::user();
//                        Obteniendo user_type del usuario
                        $user_type = $dataUser['user_type'];
//                        obteniendo tipo de rol del usuario
                        $roles = $dataUser->roles()->pluck('type');
                        $rol = $roles[0];
                        if ($user_type == 3 && $rol == 2){
                            return ['Users'=> $dataUser, 'isError'=> false, 'msgError' => ''];
                        }
                        else {
                            return ['Users'=> null, 'isError'=> true, 'msgError' => ''];
                        }
                    }
                    break;
                default:
                    echo 'no entro';
            }
        }

        else {
            return 'No hay user_social';
        }


        $validator = Validator::make($request->all(),
            [
                'email' => 'required|email',
                'password' => 'required|min:6',
            ]
        );
//
//        $credentials['email'] = $request->input('email');
//        $credentials['password'] = $request->input('password');
//
//        Auth::attempt($credentials);
//        $dataUser = Auth::user();
//        $user_type = $dataUser['user_type'];
//        $roles = $dataUser->roles()->pluck('type');
//        $rol = $roles[0];
//
//        if ($validator->fails()) {
//            $error = $validator->errors()->first();
//            return ['Users'=> null, 'isError'=> true, 'msgError' => $error];
//        }
//        else if ((Auth::attempt($credentials)) && $user_type == 1) {
//            $dataUser = Auth::user();
//            return ['Users'=> $dataUser, 'isError'=> false, 'msgError' => ''];
//        }
//        else {
//            return ['Users'=> '', 'isError'=> true, 'msgError' => 'correo cliente, password o tipo no coincide'];
//        }
    }

    //    Login chofer
    public function loginChofer(Request $request) {
        $credentials['email'] = $request->input('email');
        $credentials['password'] = $request->input('password');
        $credentials['user_type'] = $request->input('user_type');

        $validator = Validator::make($request->all(),
            [
                'email' => 'required|email',
                'password' => 'required|min:6',
                'user_type' => 'required|numeric'
            ]
        );

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return ['Users'=> null, 'isError'=> true, 'msgError' => $error];
        }
        else if ((Auth::attempt($credentials)) && ($credentials['user_type'] == '4')) {
            $dataUser = Auth::user();
            return ['Users'=> $dataUser, 'isError'=> false, 'msgError' => ''];
        }
        else {
            return ['Users'=> null, 'isError'=> true, 'msgError' => 'correo chofer, password o tipo no coincide'];
        }
    }
//    Registrar cliente
    public function registerClient (Request $request) {
        $validator = Validator::make($request->all(),
            [
                'email' => 'required|email|unique:users|max:255',
                'password' => 'required|min:6',
                'first_name' => 'required|string|max:20',
                'last_name' => 'required|string|max:20',
                'country_code' => 'required',
                'phone_number' => 'required',
                'user_type' => 'required|numeric'
            ]
        );

        // crear usuario
        $new_user = new User();
        // obtener inputs
        $new_user->type = $request->input('type','Cliente');
        $new_user->email = $request->input('email');
        $new_user->password = bcrypt($request->input('password'));
        $new_user->first_name = $request->input('first_name');
        $new_user->last_name = $request->input('last_name');
        $new_user->confirmed = $request->input('confirmed',1);
        $new_user->status= $request->input('status',1);
        $new_user->country_code = $request->input('country_code');
        $new_user->phone_number = $request->input('phone_number');
        $new_user->profile_image = $request->input('profile_image','');
        $new_user->user_type = $request->input('user_type');
        $new_user->confirmation_code = $request->input('confirmation_code', md5(uniqid(mt_rand(), true)));
        $new_user->remember_token = $request->input('remember_token',str_random(100));

        //        Guardar cliente
        $new_client = new client();

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return ['id'=> -1, 'isError'=> true, 'msgError' => ''];
        }

        else if (!$validator->fails()) {
            $new_user->save();
            $new_user->client()->save($new_client);
            return ['id'=> $new_user['id'], 'isError'=> false, 'msgError' => ''];
        }

        else {
            return ['id'=> -1, 'isError'=> true, 'msgError' => ''];
        }
    }

//    Actualizar cliente
    public function updateClient(Request $request, $id) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required',
                'first_name' => 'required|string|max:20',
                'last_name' => 'required|string|max:20',
                'password' => 'required|min:6',
                'phone_number' => 'required',
                'country_code' => 'required',
            ]
        );

        $update_cliente = User::find($id);
        $update_cliente->first_name = Input::get('first_name');
        $update_cliente->last_name = Input::get('last_name');
        $update_cliente->password = bcrypt($request->input('password'));
        $update_cliente->phone_number = Input::get('phone_number');
        $update_cliente->profile_image = Input::get('profile_image');
        $update_cliente->country_code = Input::get('country_code');
        $update_cliente->save();

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return ['id'=> -1, 'isError'=> true, 'msgError' => ''];
        }

        else if (!$validator->fails()) {
            $update_cliente->save();
            return ['id'=> $update_cliente['id'], 'isError'=> false, 'msgError' => ''];
        }

        else {
            return ['id'=> -1, 'isError'=> true, 'msgError' => ''];
        }
    }
}