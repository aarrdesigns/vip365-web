<?php

namespace App\Http\Controllers\ApiController;

use App\Models\Access\PaymentsMethod\Payments_methods;
use App\Models\Access\Trips\Trip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TripsApiController extends Controller
{
    public function index() {
        return 'index';
    }

    public function show($id) {
        return 'show';
    }

    public function getTripPay(Request $request, $id) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required'

            ]
        );

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return [
                'isPaid' => -1,
                'isError'=> true,
                'msgError' => ''
            ];
        }

        else {
            $id = $request->input('id');
            $tripPay =  Trip::find($id);
            return [
                'isPaid' => $tripPay['isPaid'],
                'isError'=> false,
                'msgError' => ''
            ];
        }
    }

    public function getTripClient(Request $request, $id) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required'

            ]
        );

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return [
                'Trips' => -1,
                'isError'=> true,
                'msgError' => ''
            ];
        }

        else {
            $id_trip = $request->input('id');
            $tripPayClient =  Trip::where('user_client_id', $id_trip)
                ->orderBy('id', 'asc')
                ->get();
            return [
                'Trips' => $tripPayClient,
                'isError'=> false,
                'msgError' => ''
            ];
        }
    }

    public function getTripDrive(Request $request, $id) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required'

            ]
        );

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return [
                'Trips' => -1,
                'isError'=> true,
                'msgError' => ''
            ];
        }

        else {
            $id_trip = $request->input('id');
            $tripPayDrive =  Trip::where('user_drive_id', $id_trip)
                ->orderBy('id', 'asc')
                ->get();
            return [
                'Trips' => $tripPayDrive,
                'isError'=> false,
                'msgError' => ''
            ];
        }
    }

    public function registerTrip (Request $request) {
        $validator = Validator::make($request->all(),
            [
                'user_client_id' => 'required',
                'user_drive_id' => 'required',
                'request_date' => 'required|date',
                'trip_date' => 'required|date',
                'origin_location' => 'required',
                'origin_name' => 'required',
                'state' => 'required',
                'trip_cost' => 'required',
                'trip_type' => 'required',
                'payment_method_id' => 'required',
                'isPaid' => 'required',
                'client_clasification' => 'required',
                'driver_clasification' => 'required'

            ]
        );

        // crear trip
        $new_trip = new Trip();

        // obtener inputs
        $new_trip->user_client_id = $request->input('user_client_id');
        $new_trip->user_drive_id = $request->input('user_drive_id');
        $new_trip->request_date = date('y-m-d', strtotime($request->input('request_date')));
        $new_trip->trip_date = date('y-m-d', strtotime($request->input('trip_date')));
        $new_trip->origin_location = $request->input('origin_location');
        $new_trip->destiny_location = $request->input('destiny_location');
        $new_trip->origin_name = $request->input('origin_name');
        $new_trip->destiny_name = $request->input('destiny_name');
        $new_trip->state = $request->input('state');
        $new_trip->path_taked = $request->input('path_taked');
        $new_trip->trip_cost = $request->input('trip_cost');
        $new_trip->trip_type = $request->input('trip_type');
        $new_trip->payment_method_id = $request->input('payment_method_id');
        $new_trip->isPaid = $request->input('isPaid');
        $new_trip->client_clasification = $request->input('client_clasification');
        $new_trip->driver_clasification = $request->input('driver_clasification');

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return ['id'=> -1, 'isError'=> true, 'msgError' => ''];
        }

        else if (!$validator->fails()) {
            $new_trip->save();
            return ['id'=> $new_trip['id'], 'isError'=> false, 'msgError' => ''];
        }

        else {
            return ['id'=> -1, 'isError'=> true, 'msgError' => ''];
        }
    }

    //    Actualizar TRIP
    public function updateTrip(Request $request, $id) {
        $validator = Validator::make($request->all(),
            [
                'id' => 'required',
                'user_client_id' => 'required',
                'user_drive_id' => 'required',
                'request_date' => 'required|date',
                'trip_date' => 'required|date',
                'origin_location' => 'required',
                'origin_name' => 'required',
                'state' => 'required',
                'trip_cost' => 'required',
                'trip_type' => 'required',
                'payment_method_id' => 'required',
                'isPaid' => 'required',
                'client_clasification' => 'required',
                'driver_clasification' => 'required'
            ]
        );

        $update_trip = Trip::find($id);

        $update_trip->user_client_id = $request->input('user_client_id');
        $update_trip->user_drive_id = $request->input('user_drive_id');
        $update_trip->request_date = date('y-m-d', strtotime($request->input('request_date')));
        $update_trip->trip_date = date('y-m-d', strtotime($request->input('trip_date')));
        $update_trip->origin_location = $request->input('origin_location');
        $update_trip->destiny_location = $request->input('destiny_location');
        $update_trip->origin_name = $request->input('origin_name');
        $update_trip->destiny_name = $request->input('destiny_name');
        $update_trip->state = $request->input('state');
        $update_trip->path_taked = $request->input('path_taked');
        $update_trip->trip_cost = $request->input('trip_cost');
        $update_trip->trip_type = $request->input('trip_type');
        $update_trip->payment_method_id = $request->input('payment_method_id');
        $update_trip->isPaid = $request->input('isPaid');
        $update_trip->client_clasification = $request->input('client_clasification');
        $update_trip->driver_clasification = $request->input('driver_clasification');
        $update_trip->save();

        if ($validator->fails()) {
            $error = $validator->errors()->first();
            return ['id'=> -1, 'isError'=> true, 'msgError' => ''];
        }

        else if (!$validator->fails()) {
            $update_trip->save();
            return ['id'=> $update_trip['id'], 'isError'=> false, 'msgError' => ''];
        }

        else {
            return ['id'=> -1, 'isError'=> true, 'msgError' => ''];
        }
    }

    public function delete(Request $request, $id) {
        return 'delete';
    }
}
