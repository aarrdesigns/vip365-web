<?php

namespace App\Http\Controllers\Backend\Access\Trips;

use App\Models\Access\Users\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Repositories\Backend\Access\Cliente\ClienteSessionRepository;

/**
 * Class ClienteSessionController.
 */
class TripsSessionController extends Controller
{
    /**
     * @param User                  $user
     * @param ManageUserRequest     $request
     * @param ClienteSessionRepository $userSessionRepository
     *
     * @return mixed
     */
    public function clearSession(User $user, ManageUserRequest $request, ClienteSessionRepository $userSessionRepository)
    {
        $userSessionRepository->clearSession($user);

        return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.session_cleared'));
    }
}
