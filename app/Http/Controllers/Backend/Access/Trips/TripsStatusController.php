<?php

namespace App\Http\Controllers\Backend\Access\Trips;

use App\Models\Access\Users\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\Cliente\ClienteRepository;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;

/**
 * Class ClienteStatusController.
 */
class TripsStatusController extends Controller
{
    /**
     * @var ClienteRepository
     */
    protected $users;

    /**
     * @param ClienteRepository $users
     */
    public function __construct(ClienteRepository $users)
    {
        $this->users = $users;
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function getDeactivated(ManageUserRequest $request)
    {
        return view('backend.access.trips.deactivated');
    }

    /**
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function getDeleted(ManageUserRequest $request)
    {
        return view('backend.access.trips.deleted');
    }

    /**
     * @param User $user
     * @param $status
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function mark(User $user, $status, ManageUserRequest $request)
    {
        $this->users->mark($user, $status);

        return redirect()->route($status == 1 ? 'admin.access.trips.index' : 'admin.access.trips.deactivated')->withFlashSuccess(trans('alerts.backend.users.updated'));
    }

    /**
     * @param User              $deletedUser
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function delete(User $deletedUser, ManageUserRequest $request)
    {
        $this->users->forceDelete($deletedUser);

        return redirect()->route('admin.access.trips.deleted')->withFlashSuccess(trans('alerts.backend.users.deleted_permanently'));
    }

    /**
     * @param User              $deletedUser
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function restore(User $deletedUser, ManageUserRequest $request)
    {
        $this->users->restore($deletedUser);

        return redirect()->route('admin.access.trips.index')->withFlashSuccess(trans('alerts.backend.users.restored'));
    }
}
