<?php

namespace App\Http\Controllers\Backend\Access\Chofer;

use App\Models\Access\Users\User;
use App\Http\Controllers\Controller;
use App\Models\Access\Users\SocialLogin;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Repositories\Backend\Access\Cliente\ClienteSocialRepository;

/**
 * Class ClienteSocialController.
 */
class ChoferSocialController extends Controller
{
    /**
     * @param User                 $user
     * @param SocialLogin          $social
     * @param ManageUserRequest    $request
     * @param ClienteSocialRepository $userSocialRepository
     *
     * @return mixed
     */
    public function unlink(User $user, SocialLogin $social, ManageUserRequest $request, ClienteSocialRepository $userSocialRepository)
    {
        $userSocialRepository->delete($user, $social);

        return redirect()->back()->withFlashSuccess(trans('alerts.backend.users.social_deleted'));
    }
}
