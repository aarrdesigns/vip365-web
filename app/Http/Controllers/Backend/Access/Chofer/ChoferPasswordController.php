<?php

namespace App\Http\Controllers\Backend\Access\Chofer;

use App\Models\Access\Users\User;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Access\Cliente\ClienteRepository;
use App\Http\Requests\Backend\Access\User\ManageUserRequest;
use App\Http\Requests\Backend\Access\User\UpdateUserPasswordRequest;

/**
 * Class ClientePasswordController.
 */
class ChoferPasswordController extends Controller
{
    /**
     * @var ClienteRepository
     */
    protected $users;

    /**
     * @param ClienteRepository $users
     */
    public function __construct(ClienteRepository $users)
    {
        $this->users = $users;
    }

    /**
     * @param User              $user
     * @param ManageUserRequest $request
     *
     * @return mixed
     */
    public function edit(User $user, ManageUserRequest $request)
    {
        return view('backend.access.chofer.change-password')
            ->withUser($user);
    }

    /**
     * @param User                      $user
     * @param UpdateUserPasswordRequest $request
     *
     * @return mixed
     */
    public function update(User $user, UpdateUserPasswordRequest $request)
    {
        $this->users->updatePassword($user, $request->only('password'));

        return redirect()->route('admin.access.chofer.index')->withFlashSuccess(trans('alerts.backend.users.updated_password'));
    }
}
