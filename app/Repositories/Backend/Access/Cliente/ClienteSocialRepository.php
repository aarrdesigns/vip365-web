<?php

namespace App\Repositories\Backend\Access\Cliente;

use App\Models\Access\Users\User;
use App\Exceptions\GeneralException;
use App\Models\Access\Users\SocialLogin;
use App\Events\Backend\Access\User\UserSocialDeleted;

/**
 * Class ClienteSocialRepository.
 */
class ClienteSocialRepository
{
    /**
     * @param User        $user
     * @param SocialLogin $social
     *
     * @return bool
     * @throws GeneralException
     */
    public function delete(User $user, SocialLogin $social)
    {
        if ($user->providers()->whereId($social->id)->delete()) {
            event(new UserSocialDeleted($user, $social));

            return true;
        }

        throw new GeneralException(trans('exceptions.backend.access.users.social_delete_error'));
    }
}
