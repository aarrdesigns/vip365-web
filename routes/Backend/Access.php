<?php

/**
 * All route names are prefixed with 'admin.access'.
 */

Route::group([
    'prefix'     => 'access',
    'as'         => 'access.',
    'namespace'  => 'Access',
    ], function () {
        /*
         * admin Management
         */
        Route::group([
            'middleware' => 'access.routeNeedsRole:1',
        ], function () {
                Route::group(['namespace' => 'User'], function () {
                /*
                 * For DataTables
                 */
                Route::post('user/get', 'UserTableController')->name('user.get');

                /*
                 * User Status'
                 */
                Route::get('user/deactivated', 'UserStatusController@getDeactivated')->name('user.deactivated');
                Route::get('user/deleted', 'UserStatusController@getDeleted')->name('user.deleted');

                /*
                 * User CRUD
                 */
                Route::resource('user', 'UserController');

                /*
                 * Specific User
                 */
                Route::group(['prefix' => 'user/{user}'], function () {
                    // Account
                    Route::get('account/confirm/resend', 'UserConfirmationController@sendConfirmationEmail')->name('user.account.confirm.resend');

                    // Status
                    Route::get('mark/{status}', 'UserStatusController@mark')->name('user.mark')->where(['status' => '[0,1]']);

                    // Social
                    Route::delete('social/{social}/unlink', 'UserSocialController@unlink')->name('user.social.unlink');

                    // Confirmation
                    Route::get('confirm', 'UserConfirmationController@confirm')->name('user.confirm');
                    Route::get('unconfirm', 'UserConfirmationController@unconfirm')->name('user.unconfirm');

                    // Password
                    Route::get('password/change', 'UserPasswordController@edit')->name('user.change-password');
                    Route::patch('password/change', 'UserPasswordController@update')->name('user.change-password.post');

                    // Access
                    Route::get('login-as', 'UserAccessController@loginAs')->name('user.login-as');

                    // Session
                    Route::get('clear-session', 'UserSessionController@clearSession')->name('user.clear-session');
                });

                /*
                 * Deleted User
                 */
                Route::group(['prefix' => 'user/{deletedUser}'], function () {
                    Route::get('delete', 'UserStatusController@delete')->name('user.delete-permanently');
                    Route::get('restore', 'UserStatusController@restore')->name('user.restore');
                });
            });

            /*
            * Role Management
            */
            Route::group(['namespace' => 'Role'], function () {
                Route::resource('role', 'RoleController', ['except' => ['show']]);

                //For DataTables
                Route::post('role/get', 'RoleTableController')->name('role.get');
            });
        });

        /*
        // * Ciente Management
        // */
        Route::group([
            'middleware' => 'access.routeNeedsRole:1',
        ], function () {
            Route::group(['namespace' => 'Cliente'], function () {
                /*
                 * For DataTables
                 */

                Route::get('cliente/get', 'ClienteTableController@')->name('cliente.gets');
                /*
                 * cliente Status'
                 */
    //            Route::get('cliente/deactivated', 'ClienteStatusController@getDeactivated')->name('cliente.deactivated');
    //            Route::get('cliente/deleted', 'ClienteStatusController@getDeleted')->name('cliente.deleted');

                /*
                 * cliente CRUD
                 */
                Route::resource('cliente', 'ClienteController');

                /*
                 * Specific cliente
                 */
    //                Route::group(['prefix' => 'cliente/{cliente}'], function () {
    //                    // Account
    //                    Route::get('account/confirm/resend', 'ClienteConfirmationController@sendConfirmationEmail')->name('cliente.account.confirm.resend');
    //
    //                    // Status
    //                    Route::get('mark/{status}', 'ClienteStatusController@mark')->name('cliente.mark')->where(['status' => '[0,1]']);
    //
    //                    // Social
    //                    Route::delete('social/{social}/unlink', 'ClienteSocialController@unlink')->name('cliente.social.unlink');
    //
    //                    // Confirmation
    //                    Route::get('confirm', 'ClienteConfirmationController@confirm')->name('user.confirm');
    //                    Route::get('unconfirm', 'ClienteConfirmationController@unconfirm')->name('user.unconfirm');
    //
    //                    // Password
    //                    Route::get('password/change', 'ClientePasswordController@edit')->name('user.change-password');
    //                    Route::patch('password/change', 'ClientePasswordController@update')->name('user.change-password.post');
    //
    //                    // Access
    //                    Route::get('login-as', 'ClienteAccessController@loginAs')->name('user.login-as');
    //
    //                    // Session
    //                    Route::get('clear-session', 'ClienteSessionController@clearSession')->name('user.clear-session');
    //                });

                /*
                 * Deleted User
                 */
    //            Route::group(['prefix' => 'cliente/{deletedUser}'], function () {
    //                Route::get('delete', 'ClienteStatusController@delete')->name('cliente.delete-permanently');
    //                Route::get('restore', 'ClienteStatusController@restore')->name('cliente.restore');
    //            });
            });
        });
        //
        ////    Chofer Management
        //
        Route::group([
            'middleware' => 'access.routeNeedsRole:1',
        ], function () {
            Route::group(['namespace' => 'Chofer'], function () {
                /*
                 * For DataTables
                 */

                Route::get('chofer/get', 'ChoferTableController@')->name('chofer.gets');
                /*
                 * cliente Status'
                 */
    //            Route::get('cliente/deactivated', 'ClienteStatusController@getDeactivated')->name('cliente.deactivated');
    //            Route::get('cliente/deleted', 'ClienteStatusController@getDeleted')->name('cliente.deleted');

                /*
                 * cliente CRUD
                 */
                Route::resource('chofer', 'ChoferController');

                /*
                 * Specific cliente
                 */
    //                Route::group(['prefix' => 'cliente/{cliente}'], function () {
    //                    // Account
    //                    Route::get('account/confirm/resend', 'ClienteConfirmationController@sendConfirmationEmail')->name('cliente.account.confirm.resend');
    //
    //                    // Status
    //                    Route::get('mark/{status}', 'ClienteStatusController@mark')->name('cliente.mark')->where(['status' => '[0,1]']);
    //
    //                    // Social
    //                    Route::delete('social/{social}/unlink', 'ClienteSocialController@unlink')->name('cliente.social.unlink');
    //
    //                    // Confirmation
    //                    Route::get('confirm', 'ClienteConfirmationController@confirm')->name('user.confirm');
    //                    Route::get('unconfirm', 'ClienteConfirmationController@unconfirm')->name('user.unconfirm');
    //
    //                    // Password
    //                    Route::get('password/change', 'ClientePasswordController@edit')->name('user.change-password');
    //                    Route::patch('password/change', 'ClientePasswordController@update')->name('user.change-password.post');
    //
    //                    // Access
    //                    Route::get('login-as', 'ClienteAccessController@loginAs')->name('user.login-as');
    //
    //                    // Session
    //                    Route::get('clear-session', 'ClienteSessionController@clearSession')->name('user.clear-session');
    //                });

                /*
                 * Deleted User
                 */
    //            Route::group(['prefix' => 'cliente/{deletedUser}'], function () {
    //                Route::get('delete', 'ClienteStatusController@delete')->name('cliente.delete-permanently');
    //                Route::get('restore', 'ClienteStatusController@restore')->name('cliente.restore');
    //            });
            });
        });
        //    Trips Managemen
        Route::group([
            'middleware' => 'access.routeNeedsRole:1',
        ], function () {
            Route::group(['namespace' => 'Trips'], function () {
                /*
                 * For DataTables
                 */

                Route::get('trips/get', 'TripsTableController')->name('trips.gets');
                /*
                 * cliente Status'
                 */
    //            Route::get('cliente/deactivated', 'ClienteStatusController@getDeactivated')->name('cliente.deactivated');
    //            Route::get('cliente/deleted', 'ClienteStatusController@getDeleted')->name('cliente.deleted');

                /*
                 * cliente CRUD
                 */
                Route::resource('trips', 'TripsController');

                /*
                 * Specific cliente
                 */
    //                Route::group(['prefix' => 'cliente/{cliente}'], function () {
    //                    // Account
    //                    Route::get('account/confirm/resend', 'ClienteConfirmationController@sendConfirmationEmail')->name('cliente.account.confirm.resend');
    //
    //                    // Status
    //                    Route::get('mark/{status}', 'ClienteStatusController@mark')->name('cliente.mark')->where(['status' => '[0,1]']);
    //
    //                    // Social
    //                    Route::delete('social/{social}/unlink', 'ClienteSocialController@unlink')->name('cliente.social.unlink');
    //
    //                    // Confirmation
    //                    Route::get('confirm', 'ClienteConfirmationController@confirm')->name('user.confirm');
    //                    Route::get('unconfirm', 'ClienteConfirmationController@unconfirm')->name('user.unconfirm');
    //
    //                    // Password
    //                    Route::get('password/change', 'ClientePasswordController@edit')->name('user.change-password');
    //                    Route::patch('password/change', 'ClientePasswordController@update')->name('user.change-password.post');
    //
    //                    // Access
    //                    Route::get('login-as', 'ClienteAccessController@loginAs')->name('user.login-as');
    //
    //                    // Session
    //                    Route::get('clear-session', 'ClienteSessionController@clearSession')->name('user.clear-session');
    //                });

                /*
                 * Deleted User
                 */
    //            Route::group(['prefix' => 'cliente/{deletedUser}'], function () {
    //                Route::get('delete', 'ClienteStatusController@delete')->name('cliente.delete-permanently');
    //                Route::get('restore', 'ClienteStatusController@restore')->name('cliente.restore');
    //            });
            });
        });
});
