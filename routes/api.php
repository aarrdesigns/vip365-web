<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// -----   Login CHOFER  ------
//  POST http://localhost:8888/vip365-web/api/loginChofer
//  email, password, user_type
Route::post('loginChofer','ApiController\AuthApiController@loginChofer');

// -----   Login CLIENTE  ------
//  POST http://localhost:8888/vip365-web/api/loginClient
//  email, password, user_type
    Route::post('loginClient','ApiController\AuthApiController@loginClient');

// -----   Registrar CLIENTE ------
//  POST http://localhost:8888/vip365-web/api/registerClient
//  email, password, user_type, first_name, last_name, phone_number, country_code
    Route::post('registerClient','ApiController\AuthApiController@registerClient');

// ------ Actualizar CLIENTE ------
//  PUT http://localhost:8888/vip365-web/api/updateClient/id
//  POST MAN x-www-form-urlencoded
//  id, profile_image, first_name, last_name, phone_number, password
//    Route::put('updateClient/{id}','ApiController\AuthApiController@update');
// -----------------------------------------------------------------------------
//  POST http://localhost:8888/vip365-web/api/updateClient/id
//  id, profile_image, first_name, last_name, phone_number, password
    Route::post('updateClient/{id}','ApiController\AuthApiController@updateClient');

// ------ Registrar TRIP -------
//  POST http://localhost:8888/vip365-web/api/registerTrip
//  user_client_id, user_drive_id, request_date, trip_date, origin_location, origin_name, state, trip_cost, trip_type, payment_method_id, isPaid, client_clasification, driver_clasification
    Route::post('registerTrip','ApiController\TripsApiController@registerTrip');

// ------ Actualizar TRIP -------
//  POST http://localhost:8888/vip365-web/api/updateTrip/id
//  id user_client_id, user_drive_id, request_date, trip_date, origin_location, origin_name, state, trip_cost, trip_type, payment_id, isPaid, client_clasification, driver_clasification
  Route::post('updateTrip/{id}','ApiController\TripsApiController@updateTrip');

// ------ get TRIP PAY --------
//  POST http://localhost:8888/vip365-web/api/getTripPay/id
//  id => trip_id id del trip
    Route::post('getTripPay/{id}','ApiController\TripsApiController@getTripPay');

// ------ get TRIP CLIENT --------
//  POST http://localhost:8888/vip365-web/api/getTripClient/id
//  id => user_client_id id del cliente
    Route::post('getTripClient/{id}','ApiController\TripsApiController@getTripClient');

// ------ get TRIP DRIVE --------
//  POST http://localhost:8888/vip365-web/api/getTripDrivet/id
//  user_drive_id
    Route::post('getTripDrive/{id}','ApiController\TripsApiController@getTripDrive');

// ------ Obtener DRIVER -----
//  POST http://localhost:8888/vip365-web/api/getDriver/id
// id del driver
    Route::post('getDriver/{id}','ApiController\DriversApiController@getDriver');

// ------ Actualizar DRIVER -----
//  POST http://localhost:8888/vip365-web/api/updateDriver/id
//  id, current_location
    Route::post('updateDriver/{id}','ApiController\DriversApiController@updateDriver');


// ------ obtener NEAR DRIVER -----
//  POST http://localhost:8888/vip365-web/api/getNearDriver/id
//  id, driver_past_location, driver_new_location
    Route::post('getNearDriver','ApiController\DriversApiController@getNearDriver');

// ------ registrar PAYMENTS METHODS ------
//  POST http://localhost:8888/vip365-web/api/registerPaymentsMethods
//  user_id, card_type, card_number, card_security_number, paypal_code card_holder_name, expDate, payment_type, isDefault, isActive
    Route::post('registerPaymentsMethods','ApiController\PaymentsMethodsApiController@registerPaymentsMethods');

// ------ Actualizar PAYMENTS METHODS -----
//  POST http://localhost:8888/vip365-web/api/updatePaymentsMethods/id
//  id, current_location
    Route::post('updatePaymentsMethods/{id}','ApiController\DriversApiController@updatePaymentsMethods');

// ----- Obtener PAYMENTS METHODS -----
// POST http://localhost:8888/vip365-web/api/getPaymentsMethods/id
// id del payments methods
    Route::post('getPaymentsMethods/{id}','ApiController\PaymentsMethodsApiController@getPaymentsMethods');

// ----- Obtener CURRENT SETTINGS -----
// POST http://localhost:8888/vip365-web/api/getSettings
    Route::post('getSettings','ApiController\SettingsApiController@getSettings');