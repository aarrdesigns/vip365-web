<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments_methods', function (Blueprint $table) {
            $table->increments('id')->comments('id payments');
            $table->integer('user_id')->unsigned()->comments('id usuario')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->string('card_type')->comments('tipo tarjeta')->nullable();
            $table->string('card_number')->comments('numero tarjeta')->nullable();
            $table->string('card_security_number')->comments('numero de seguridad de la tarjeta')->nullable();
            $table->string('card_holder_name')->comments('nombre del titular de la tarjeta')->nullable();
            $table->string('expDate')->comments('fecha de expiracion ej: 10/18')->nullable();
            $table->integer('payment_type')->comments('tipo de pago 1: tarjeta credito/debito 2:PayPal')->nullable();
            $table->string('paypal_code')->comments('codigo de autorización de PayPal')->nullable();
            $table->boolean('isDefault')->comments('Si el pago esta por defecto')->nullable();
            $table->boolean('isActive')->comments('Metodo de pago activo, campo para eliminacion logica')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments_methods');
    }
}
