<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->increments('id')->comments('id pago');
            $table->integer('user_id')->unsigned()->comments('id usuario')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('car_id')->unsigned()->comments('id carro')->nullable();
            $table->foreign('car_id')->references('id')->on('cars')->onDelete('cascade')->onUpdate('cascade');
            $table->string('driver_past_location')->comments('ubicacion anterior lat, lng del conductor')->nullable();
            $table->string('driver_new_location')->comments('ubicacion nueva lat, lng del conductor')->nullable();
            $table->integer('driver_state')->comments('estado del conductor 1: en camino 2: en viaje 3: activo 4:inactivo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
