<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id')->comments('id pago');
            $table->integer('user_id')->unsigned()->comments('id usuario')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('trip_id')->unsigned()->comments('id viaje')->nullable();
            $table->foreign('trip_id')->references('id')->on('trips')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('payment_method_id')->unsigned()->comments('id metodo de pago')->nullable();
            $table->foreign('payment_method_id')->references('id')->on('payments_methods')->onDelete('cascade')->onUpdate('cascade');
            $table->double('amount')->comments('monto del pago')->nullable();
            $table->boolean('isApproved')->comments('el pago fue aprobado')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
