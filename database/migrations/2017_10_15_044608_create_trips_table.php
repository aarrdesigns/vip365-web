<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id')->comments('id viaje');
//            Relación
            $table->integer('user_client_id')->unsigned()->comments('id usuario cliente')->nullable();
            $table->foreign('user_client_id')->references('id')->on('clients')->onDelete('cascade')->onUpdate('cascade');
            //            Relación
            $table->integer('user_drive_id')->unsigned()->comments('id usuario chofer')->nullable();
            $table->foreign('user_drive_id')->references('id')->on('drivers')->onDelete('cascade')->onUpdate('cascade');
//            Relacione
            $table->integer('payment_method_id')->unsigned()->comments('id del pago seleccionado')->nullable();
            $table->foreign('payment_method_id')->references('id')->on('payments_methods')->onDelete('cascade')->onUpdate('cascade');
            $table->date('request_date')->comments('fecha de solicitud')->nullable();
            $table->date('trip_date')->comments('fecha de viaje')->nullable();
            $table->string('origin_location')->comments('ubicación de origen')->nullable();
            $table->string('destiny_location')->comments('ubicación de destino')->nullable();
            $table->string('origin_name')->comments('nombre de origen')->nullable();
            $table->string('destiny_name')->comments('nombre de destino')->nullable();
            $table->integer('state')->comments('estado de viaje 1: pendiente 2: en curso 3: completado')->nullable();
            $table->string('path_taked')->comments('camino tomado')->nullable();
            $table->double('trip_cost')->comments('costo viaje')->nullable();
            $table->integer('trip_type')->comments('tipo de viaje 1: normal 2: emergencia 3: alerta')->nullable();
            $table->boolean('isPaid')->comments('Para validar si el viaje se pago');
            $table->integer('client_clasification')->comments('calificacion del cliente')->nullable();
            $table->integer('driver_clasification')->comments('calificacion del chofer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
    }
}
