<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->increments('id')->comments('id carro');
            $table->string('car_color')->comments('color del carro')->nullable();
            $table->string('car_plaque')->comments('placa del carro')->nullable();
            $table->string('car_model')->comments('modelo del carro')->nullable();
            $table->string('car_brand')->comments('marca del carro')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cars');
    }
}
