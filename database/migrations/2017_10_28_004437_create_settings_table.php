<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id')->comments('id de configuracion');
            $table->double('distance_rate')->comments('costo por distancia');
            $table->double('minute_rate')->comments('costo por minuto');
            $table->double('minimum_rate')->comments('costo por minimo');
            $table->double('base_rate')->comments('costo base');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
