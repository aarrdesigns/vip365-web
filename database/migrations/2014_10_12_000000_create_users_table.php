<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class CreateUsersTable.
 */
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->comment('id usuario');
//            $table->enum('type',['Administrador','Cliente','Usuario','Chofer','Administrativo'])->comment('tipo de rol');
            $table->string('first_name')->comment('nombre usuario')->nullable();
            $table->string('last_name')->comment('apellido usuario')->nullable();
            $table->string('email')->comment('correo usuario')->unique()->nullable();
            $table->integer('user_type')->comment('tipo de usuario 1:normal 2:google 3:facebook')->nullable();
            $table->string('password')->comment('contrasena')->nullable();
            $table->string('phone_number')->comment('telefono')->nullable();
            $table->string('profile_image')->comment('imagen de perfil')->nullable();
            $table->string('country_code')->comment('codigo de pais de 2 letras')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
