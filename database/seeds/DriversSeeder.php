<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Database\DisableForeignKeys;

class DriversSeeder extends Seeder
{
    use DisableForeignKeys;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        DB::table('drivers')->insert([
            'user_id' => 1,
            'car_id' => 1,
//            MERIDA
            'driver_past_location' => '20.9800512,-89.7029587',
//            CARABOBO
            'driver_new_location' => '10.2115334,-68.2113086',
            'driver_state' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $this->enableForeignKeys();
    }
}
