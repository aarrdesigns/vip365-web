<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CarsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert([
            'car_color' => 'azul',
            'car_plaque' => '245X-525',
            'car_model' => 'focus',
            'car_brand' => 'ford',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
