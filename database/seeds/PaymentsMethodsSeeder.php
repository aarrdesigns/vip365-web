<?php

use Carbon\Carbon;
use Database\DisableForeignKeys;
use Illuminate\Database\Seeder;

class PaymentsMethodsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    use DisableForeignKeys;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        DB::table('payments_methods')->insert([
            'user_id' => 1,
            'card_type' => 'visa',
            'card_number' => '1485934693461024',
            'card_security_number' => '353',
            'card_holder_name' => 'Victor Silva',
            'expDate' => '07/22',
            'payment_type' => '1',
            'paypal_code' => '1234gll35l35',
            'isDefault' => '1',
            'isActive' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
        $this->enableForeignKeys();
    }
}
