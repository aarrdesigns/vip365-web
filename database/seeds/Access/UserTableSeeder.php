<?php

use Database\TruncateTable;
use Carbon\Carbon as Carbon;
use Illuminate\Database\Seeder;
use Database\DisableForeignKeys;
use Illuminate\Support\Facades\DB;

/**
 * Class UserTableSeeder.
 */
class UserTableSeeder extends Seeder
{
    use DisableForeignKeys, TruncateTable;

    /**
     * Run the database seed.
     *
     * @return void
     */
    public function run()
    {
        $this->disableForeignKeys();
        $this->truncateMultiple([config('access.users_table'), 'social_logins']);

        //Add the master administrator, user id of 1
        $users = [
            [
                'first_name'        => 'Admin',
                'last_name'         => 'Admin',
                'user_type'         => '1',
//                'type'              => 'Administrador',
                'email'             => 'admin@vip365.com',
                'country_code'      => 've',
                'phone_number'      => '2434242342',
                'profile_image'     => '',
                'password'          => bcrypt('123456'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
                'remember_token'    => str_random(100),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'first_name'        => 'Alejandro',
                'last_name'         => 'Guzman',
                'user_type'         => '1',
//                'type'              => 'Cliente',
                'email'             => 'cliente@vip365.com',
                'country_code'      => 've',
                'phone_number'      => '2434242342',
                'profile_image'     => '',
                'password'          => bcrypt('123456'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
                'remember_token'    => str_random(100),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'first_name'        => 'Luis',
                'last_name'         => 'Hernandez',
                'user_type'         => '2',
//                'type'              => 'Cliente',
                'email'             => 'cliente-google@vip365.com',
                'country_code'      => 've',
                'phone_number'      => '2434234234',
                'profile_image'     => '',
                'password'          => bcrypt('123456'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
                'remember_token'    => str_random(100),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'first_name'        => 'Maria',
                'last_name'         => 'Hernandez',
                'user_type'         => '3',
//                'type'              => 'Cliente',
                'email'             => 'cliente-facebook@vip365.com',
                'country_code'      => 'cr',
                'phone_number'      => '224799798',
                'profile_image'     => '',
                'password'          => bcrypt('123456'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
                'remember_token'    => str_random(100),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'first_name'        => 'Andrea',
                'last_name'         => 'Hernandez',
                'user_type'         => '3',
//                'type'              => 'Usuario',
                'email'             => 'usuario@vip365.com',
                'country_code'      => 'mx',
                'phone_number'      => '242786768',
                'profile_image'     => '',
                'password'          => bcrypt('123456'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
                'remember_token'    => str_random(100),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'first_name'        => 'Jesus',
                'last_name'         => 'Antonio',
                'user_type'         => '1',
//                'type'              => 'Chofer',
                'email'             => 'chofer@vip365.com',
                'country_code'      => 've',
                'phone_number'      => '2434242342',
                'profile_image'     => '',
                'password'          => bcrypt('123456'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
                'remember_token'    => str_random(100),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
            [
                'first_name'        => 'Richard',
                'last_name'         => 'Hidalgo',
                'user_type'         => '1',
//                'type'              => 'Administrativo',
                'email'             => 'administrativo@vip365.com',
                'country_code'      => 've',
                'phone_number'      => '2434242342',
                'profile_image'     => '',
                'password'          => bcrypt('123456'),
                'confirmation_code' => md5(uniqid(mt_rand(), true)),
                'confirmed'         => true,
                'remember_token'    => str_random(100),
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ],
        ];

        DB::table(config('access.users_table'))->insert($users);

        $this->enableForeignKeys();
    }
}
