<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon as Carbon;

class SettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'distance_rate' => '3.85',
            'minute_rate' => '1.70',
            'minimum_rate' => '25',
            'base_rate' => '6.40',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
        ]);
    }
}
