<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menus Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in menu items throughout the system.
    | Regardless where it is placed, a menu item can be listed here so it is easily
    | found in a intuitive way.
    |
    */

    'backend' => [
        'access' => [
            'title-users' => 'Usuarios',
            'title-clients' => 'Clientes',
            'title-choferes' => 'Choferes',
            'title-finanzas' => 'Finanzas',
            'title-charts' => 'Graficas',
            'title-trips' => 'Viajes',

            'roles' => [
                'all'        => 'Todos los Roles',
                'create'     => 'Nuevo Rol',
                'edit'       => 'Modificar Rol',
                'management' => 'Administración de Roles',
                'main'       => 'Roles',
            ],

            'users' => [
                'all'             => 'Ver usuarios',
                'change-password' => 'Cambiar la contraseña',
                'create'          => 'Crear Usuario',
                'deactivated'     => 'Desactivados',
                'deleted'         => 'Eliminados',
                'edit'            => 'Modificar Usuario',
                'main'            => 'Usuario',
                'view'            => 'Ver Usuario',
            ],
        ],

        'log-viewer' => [
            'main'      => 'Gestór de Logs',
            'dashboard' => 'Principal',
            'logs'      => 'Logs',
        ],

        'sidebar' => [
            'dashboard' => 'Principal',
            'general'   => 'General',
            'clients'   => 'Clientes',
            'payments'   => 'Finanzas',
            'chart'   => 'Gráficas',
            'system'    => 'Sistema',
        ],
    ],

    'language-picker' => [
        'language' => 'ES',
        /*
         * Add the new language to this array.
         * The key should have the same language code as the folder name.
         * The string should be: 'Language-name-in-your-own-language (Language-name-in-English)'.
         * Be sure to add the new language in alphabetical order.
         */
        'langs' => [
//            'ar'    => 'العربية (Arabic)',
//            'zh'    => '(Chinese Simplified)',
//            'zh-TW' => '(Chinese Traditional)',
//            'da'    => 'Danés (Danish)',
//            'de'    => 'Alemán (German)',
//            'el'    => '(Greek)',
            'en'    => 'Inglés (English)',
            'es'    => 'Español (Spanish)',
//            'fr'    => 'Francés (French)',
//            'id'    => 'Indonesio (Indonesian)',
//            'it'    => 'Italiano (Italian)',
//            'ja'    => '(Japanese)',
//            'nl'    => 'Holandés (Dutch)',
//            'pt_BR' => 'Portugués Brasileño',
//            'ru'    => 'Russian (Russian)',
//            'sv'    => 'Sueco (Swedish)',
//            'th'    => '(Thai)',
//            'tr'    => '(Turkish)',
        ],
    ],
    'landing-page' => [
        'home' => 'Inicio',
        'how-it-work' => 'Como funciona',
        'features' => 'Caracteristicas',
        'download' => 'Descarga',
        'contact' => 'Contactanos',
    ],
];
