/**
 * Allows you to add data-method="METHOD to links to automatically inject a form
 * with the method on click
 *
 * Example: <a href="{{route('customers.destroy', $customer->id)}}"
 * data-method="delete" name="delete_item">Delete</a>
 *
 * Injects a form with that's fired on click of the link with a DELETE request.
 * Good because you don't have to dirty your HTML with delete forms everywhere.
 */
function addDeleteForms() {
    $('[data-method]').append(function () {
        if (! $(this).find('form').length > 0)
            return "\n" +
                "<form action='" + $(this).attr('href') + "' method='POST' name='delete_item' style='display:none'>\n" +
                "<input type='hidden' name='_method' value='" + $(this).attr('data-method') + "'>\n" +
                "<input type='hidden' name='_token' value='" + $('meta[name="csrf-token"]').attr('content') + "'>\n" +
                "</form>\n";
        else
            return "";
    })
        .removeAttr('href')
        .attr('style', 'cursor:pointer;')
        .attr('onclick', '$(this).find("form").submit();');
}

/**
 * Place any jQuery/helper plugins in here.
 */

// $(function(){
//     $('#world-map').vectorMap({
//         map: 'world_mill',
//         scaleColors: ['#C8EEFF', '#0071A4'],
//         normalizeFunction: 'polynomial',
//         hoverOpacity: 0.7,
//         hoverColor: false,
//         markerStyle: {
//             initial: {
//                 fill: '#F8E23B',
//                 stroke: '#383f47'
//             }
//         },
//         backgroundColor: '#383f47',
//         markers: [
//             {latLng: [41.90, 12.45], name: 'Vatican City'},
//             {latLng: [43.73, 7.41], name: 'Monaco'},
//             {latLng: [-0.52, 166.93], name: 'Nauru'},
//             {latLng: [-8.51, 179.21], name: 'Tuvalu'},
//             {latLng: [43.93, 12.46], name: 'San Marino'},
//             {latLng: [47.14, 9.52], name: 'Liechtenstein'},
//             {latLng: [7.11, 171.06], name: 'Marshall Islands'},
//             {latLng: [17.3, -62.73], name: 'Saint Kitts and Nevis'},
//             {latLng: [3.2, 73.22], name: 'Maldives'},
//             {latLng: [35.88, 14.5], name: 'Malta'},
//             {latLng: [12.05, -61.75], name: 'Grenada'},
//             {latLng: [13.16, -61.23], name: 'Saint Vincent and the Grenadines'},
//             {latLng: [13.16, -59.55], name: 'Barbados'},
//             {latLng: [17.11, -61.85], name: 'Antigua and Barbuda'},
//             {latLng: [-4.61, 55.45], name: 'Seychelles'},
//             {latLng: [7.35, 134.46], name: 'Palau'},
//             {latLng: [42.5, 1.51], name: 'Andorra'},
//             {latLng: [14.01, -60.98], name: 'Saint Lucia'},
//             {latLng: [6.91, 158.18], name: 'Federated States of Micronesia'},
//             {latLng: [1.3, 103.8], name: 'Singapore'},
//             {latLng: [1.46, 173.03], name: 'Kiribati'},
//             {latLng: [-21.13, -175.2], name: 'Tonga'},
//             {latLng: [15.3, -61.38], name: 'Dominica'},
//             {latLng: [-20.2, 57.5], name: 'Mauritius'},
//             {latLng: [26.02, 50.55], name: 'Bahrain'},
//             {latLng: [0.33, 6.73], name: 'São Tomé and Príncipe'}
//         ]
//     });
// });


$(function(){
    let $loading = $('.loader');

    $(document).ajaxStart(function () {
        $loading.show();
    }).ajaxError(function (event, jqxhr, settings, thrownError) {
        $loading.hide();
        location.reload();
    }).ajaxStop(function () {
        $loading.hide();
    }).on('draw.dt', function() {
        addDeleteForms();
    });

    /**
     * Add the data-method="delete" forms to all delete links
     */
    addDeleteForms();

    /**
     * Place the CSRF token as a header on all pages for access in AJAX requests
     */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * Bind all bootstrap tooltips & popovers
     */
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();

    /**
     * Generic confirm form delete using Sweet Alert
     */
    $('body').on('submit', 'form[name=delete_item]', function(e){
        e.preventDefault();

        let form = this,
            link = $('a[data-method="delete"]'),
            cancel = (link.attr('data-trans-button-cancel')) ? link.attr('data-trans-button-cancel') : "Cancel",
            confirm = (link.attr('data-trans-button-confirm')) ? link.attr('data-trans-button-confirm') : "Yes, delete",
            title = (link.attr('data-trans-title')) ? link.attr('data-trans-title') : "Warning",
            text = (link.attr('data-trans-text')) ? link.attr('data-trans-text') : "Are you sure you want to delete this item?";

        swal({
            title: title,
            type: "warning",
            showCancelButton: true,
            cancelButtonText: cancel,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: confirm,
            closeOnConfirm: true
        }, function(confirmed) {
            if (confirmed)
                form.submit();
        });
    }).on('click', 'a[name=confirm_item]', function(e){
        /**
         * Generic 'are you sure' confirm box
         */
        e.preventDefault();

        let link = $(this),
            title = (link.attr('data-trans-title')) ? link.attr('data-trans-title') : "Are you sure you want to do this?",
            cancel = (link.attr('data-trans-button-cancel')) ? link.attr('data-trans-button-cancel') : "Cancel",
            confirm = (link.attr('data-trans-button-confirm')) ? link.attr('data-trans-button-confirm') : "Continue";

        swal({
            title: title,
            type: "info",
            showCancelButton: true,
            cancelButtonText: cancel,
            confirmButtonColor: "#3C8DBC",
            confirmButtonText: confirm,
            closeOnConfirm: true
        }, function(confirmed) {
            if (confirmed)
                window.location = link.attr('href');
        });
    }).on('click', function (e) {
        /**
         * This closes popovers when clicked away from
         */
        $('[data-toggle="popover"]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });
});
