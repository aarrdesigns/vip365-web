{{--Header--}}
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#home"><img class="logo" src="{{asset('/public/images/ic_vip.png')}}"></a>
        {{--<a href="#" class="btn btn-outline button-yellow btn-xl js-scroll-trigger">Become a driver</a>--}}
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">

            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#home">{{ trans('menus.landing-page.home') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#how-it-work">{{ trans('menus.landing-page.how-it-work') }}</a>
                </li>
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link js-scroll-trigger" href="#features">{{ trans('menus.landing-page.features') }}</a>--}}
                {{--</li>--}}
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#download">{{ trans('menus.landing-page.download') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#contact">{{ trans('menus.landing-page.contact') }}</a>
                </li>
                {{--<li class="nav-item">--}}
                    {{--<a class="nav-link js-scroll-trigger" href="{{ route('login') }}">SIGN IN</a>--}}
                {{--</li>--}}
                {{--@if (config('locale.status') && count(config('locale.languages')) > 1)--}}
                    {{--<li class="dropdown">--}}
                        {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">--}}
                            {{--{{ trans('menus.language-picker.language') }}--}}
                            {{--<span class="caret"></span>--}}
                        {{--</a>--}}

                        {{--@include('includes.partials.lang')--}}
                    {{--</li>--}}
                {{--@endif--}}

                {{--@if ($logged_in_user)--}}
                    {{--<li class="nav-item login-btn">{{ link_to_route('frontend.user.dashboard', trans('navs.frontend.dashboard'), [], ['class' => active_class(Active::checkRoute('frontend.user.dashboard')) ]) }}</li>--}}
                {{--@endif--}}

                @if (! $logged_in_user)
                    <li class="nav-item login-btn">{{ link_to_route('frontend.auth.login', trans('navs.frontend.login'), [], ['class' => active_class(Active::checkRoute('frontend.auth.login')) ], ['class' => 'nav-link']) }}</li>

                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ $logged_in_user->email }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @permission('view-backend')
                            <li class="dropdown-item">{{ link_to_route('admin.dashboard', trans('navs.frontend.user.administration')) }}</li>
                            @endauth
                            {{--<li class="dropdown-item">{{ link_to_route('frontend.user.account', trans('navs.frontend.user.account'), [], ['class' => active_class(Active::checkRoute('frontend.user.account')) ]) }}</li>--}}
                            <div class="dropdown-divider"></div>
                            <li class="dropdown-item"><i class="fa fa-sign-out"></i> {{ link_to_route('frontend.auth.logout', trans('navs.general.logout')) }}</li>
                        </ul>
                    </li>
                @endif

            </ul>
        </div>
    </div>
</nav>