{{--Footer--}}
<footer>
    <div class="container">
        <p>&copy; 2017 Vip365. All Rights Reserved.</p>
        <ul class="list-inline">
            <li class="list-inline-item">
                <a href="#">Privacy</a>
            </li>
            <li class="list-inline-item">
                <a href="#">Terms</a>
            </li>
            <li class="list-inline-item">
                <a href="#">FAQ</a>
            </li>
            {{--@if (config('locale.status') && count(config('locale.languages')) > 1)--}}
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">--}}
                        {{--{{ trans('menus.language-picker.language') }}--}}
                        {{--<span class="caret"></span>--}}
                    {{--</a>--}}

                    {{--@include('includes.partials.lang')--}}
                {{--</li>--}}
            {{--@endif--}}
        </ul>
        <ul class="list-inline">
            <li class="list-inline-item">
                <div class='to-top'>
                    <a class='back-to-top fa fa-chevron-up' href='#'></a>
                </div>
            </li>
        </ul>
    </div>
</footer>
{{--Scripts--}}
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="{{asset('public/js/popper.js')}}"></script>
<script src="{{asset('public/js/bootstrap.js')}}"></script>
<script src="{{asset('public/js/jquery.easing.js')}}"></script>
{{--<script src="{{asset('/js/app.js')}}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/skrollr/0.6.30/skrollr.min.js"></script>
<script>
    (function($) {
        "use strict"; // Start of use strict
        // Smooth scrolling using jQuery easing
        $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html, body').animate({
                        scrollTop: (target.offset().top - 48)
                    }, 1000, "easeInOutExpo");
                    return false;
                }
            }
        });

        // Closes responsive menu when a scroll trigger link is clicked
        $('.js-scroll-trigger').click(function() {
            $('.navbar-collapse').collapse('hide');
        });
        // Activate scrollspy to add active class to navbar items on scroll
        $('body').scrollspy({
            target: '#mainNav',
            offset: 54
        });
        // Collapse the navbar when page is scrolled
        $(window).scroll(function() {
            if ($("#mainNav").offset().top > 100) {
                $("#mainNav").addClass("navbar-shrink");
            } else {
                $("#mainNav").removeClass("navbar-shrink");
            }
        });
    })(jQuery); // End of use strict
</script>
<script>
//    skrollr
    skrollr.init({
        forceHeight: false
    });
//    scroll
$(function () {

    var offset = 300,
        duration = 500,
        top_section = $('.to-top'),
        toTopButton = $('a.back-to-top');
    // showing and hiding button according to scroll amount (in pixels)
//    $(window).scroll(function () {
//        if( $(this).scrollTop() > offset ) {
//            $(top_section).fadeIn(duration);
//        } else{
//            $(top_section).fadeOut(duration);
//
//        }});

    // activate smooth scroll to top when clicking on the button

    $(toTopButton).click(function(e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 700);
    });

});
</script>