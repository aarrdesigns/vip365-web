@extends('frontend.layouts.app')

@section('title', app_name() . ' | Login')

@section('content')

    <style>
        body {
            /*background: #003781;*/
            font-family: "Roboto", sans-serif;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            width: 100%;
            height: 100%;
            background: url('public/images/login.jpg') center center no-repeat;
            background-size: cover;
        }

        body:before {
             content: '';
             position: absolute;
             top: 0;
             right: 0;
             bottom: 0;
             left: 0;
             background-image: linear-gradient(to bottom right,#002f4b,#495057);
             opacity: .6;
         }

        .login .vertical-center {
            display: flex;
            align-items: center;
            min-height: 100vh;
        }
        .login .form {
            position: relative;
            z-index: 1;
            background: #FFFFFF;
            max-width: 300px;
            margin: 0 auto 100px;
            padding: 30px;
            border-radius: 2%;
            box-shadow: 2px 0 0 0 #ccc;
        }
        .login .form .thumbnail {
            width: 100px;
            margin: 0 auto 15%;
            box-sizing: border-box;
        }
        .login .form .thumbnail img {
            display: block;
            width: 100%;
        }
        .login .form input {
            background: #f8f9fa;
            width: 100%;
            margin: 0 0 15px;
            padding: 15px;
            border-radius: 2%;
            border: 1px solid #ccc;
            box-sizing: border-box;
            font-size: 12px;
        }
        .login .form .btn-primary {
            background: #003781;
            border: 0;
            padding: 15px;
            border-radius: 3px;
            color: #FFFFFF;
            font-size: 12px;
            font-weight: 600;
            -webkit-transition: all 0.3s ease;
            transition: all 0.3s ease;
            cursor: pointer;
        }

        .login a.reset {
            color: #003781;
        }
        .login a.reset:hover {
            color:#001f3f;
        }
    </style>

    <div class="login container">
        <div class="row vertical-center">
            {{--<div class="form">--}}
                {{ Form::open(['route' => 'frontend.auth.login.post', 'class' => 'form']) }}
                    <div class="thumbnail"><img src="{{asset('public/images/ic_vip.png')}}"/></div>
                    {{--<form class="login-form">--}}
                        {{--<input type="text" placeholder="username"/>--}}
                        {{ Form::email('email', null, ['class' => '', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.email')]) }}
                        {{--<input type="password" placeholder="password"/>--}}
                        {{ Form::password('password', ['class' => '', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password')]) }}
                        {{--{{ Form::checkbox('remember') }} {{ trans('labels.frontend.auth.remember_me') }}--}}
                        {{--<button>login</button>--}}
                        {{ Form::submit(trans('labels.frontend.auth.login_button'), ['class' => 'btn btn-primary']) }}

{{--                        {{ link_to_route('frontend.auth.password.reset', trans('labels.frontend.passwords.forgot_password'), [], ['class' => 'reset']) }}--}}
                        <div class="row text-center">
                            {!! $socialite_links !!}
                        </div>
                        {{--<p class="message">Not registered? <a href="#">Create an account</a></p>--}}
                    {{--</form>--}}
                {{ Form::close() }}
            {{--</div>--}}
        </div>
    </div>

    {{--<div class="panel-body">--}}

        {{--{{ Form::open(['route' => 'frontend.auth.login.post', 'class' => 'form-horizontal']) }}--}}

        {{--<div class="form-group">--}}
            {{--{{ Form::label('email', trans('validation.attributes.frontend.email'), ['class' => 'col-md-4 control-label']) }}--}}
            {{--<div class="col-md-6">--}}
                {{--{{ Form::email('email', null, ['class' => 'form-control', 'maxlength' => '191', 'required' => 'required', 'autofocus' => 'autofocus', 'placeholder' => trans('validation.attributes.frontend.email')]) }}--}}
            {{--</div><!--col-md-6-->--}}
        {{--</div><!--form-group-->--}}

        {{--<div class="form-group">--}}
            {{--{{ Form::label('password', trans('validation.attributes.frontend.password'), ['class' => 'col-md-4 control-label']) }}--}}
            {{--<div class="col-md-6">--}}
                {{--{{ Form::password('password', ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('validation.attributes.frontend.password')]) }}--}}
            {{--</div><!--col-md-6-->--}}
        {{--</div><!--form-group-->--}}

        {{--<div class="form-group">--}}
            {{--<div class="col-md-6 col-md-offset-4">--}}
                {{--<div class="checkbox">--}}
                    {{--<label>--}}
                        {{--{{ Form::checkbox('remember') }} {{ trans('labels.frontend.auth.remember_me') }}--}}
                    {{--</label>--}}
                {{--</div>--}}
            {{--</div><!--col-md-6-->--}}
        {{--</div><!--form-group-->--}}

        {{--<div class="form-group">--}}
            {{--<div class="col-md-6 col-md-offset-4">--}}
                {{--{{ Form::submit(trans('labels.frontend.auth.login_button'), ['class' => 'btn btn-primary', 'style' => 'margin-right:15px']) }}--}}

                {{--{{ link_to_route('frontend.auth.password.reset', trans('labels.frontend.passwords.forgot_password')) }}--}}
            {{--</div><!--col-md-6-->--}}
        {{--</div><!--form-group-->--}}

        {{--{{ Form::close() }}--}}

        {{--<div class="row text-center">--}}
            {{--{!! $socialite_links !!}--}}
        {{--</div>--}}
    {{--</div><!-- panel body -->--}}

@endsection