@extends('frontend.layouts.app')

@section('content')
    @include('frontend.includes.nav')
    {{--Home--}}
    <header class="masthead" id="home">
        <div class="container h-100">
            <div class="row h-100">
                <div class="col-lg-8 my-auto">
                    <div class="header-content mx-auto">
                        <h1 class="mb-30" data-100="opacity:1;" data-200=" opacity:0">TELL US WHERE<br> WE ARE GOING <br>WE WILL FIND YOU</h1>
                        <a href="#download" data-100="margin-top:0; opacity:1;" data-200="margin-top:30%; opacity:0;" class="btn btn-outline btn-xl js-scroll-trigger">Let's do it!</a>
                    </div>
                </div>
                <div class="col-lg-4 my-auto">
                    <div class="device-container" data-100="opacity:1;" data-300="opacity:0;">
                        <div class="device-mockup iphone6 portrait white">
                            <div class="device">
                                <div class="screen">
                                    <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                                    <img src="" class="img-fluid" alt="">
                                </div>
                                <div class="button">
                                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    {{--How it works--}}
    <section class="features" id="how-it-work">
        <div class="container">
            <div class="section-heading text-center" data-400="opacity:0;" data-750="opacity:1;">
                <h2>How it works</h2>
                <p class="text-muted">Check out what you can do with this app theme!</p>
                <hr>
            </div>
            <div class="row">
                <div class="col-lg-4 my-auto">
                    <div class="device-container" data-450="opacity:0; left: -130%;position: relative;" data-650="left:0; opacity:1">
                        <div class="device-mockup iphone6 portrait white">
                            <div class="device">
                                <div class="screen">
                                    <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                                    <img src="" class="img-fluid" alt="">
                                </div>
                                <div class="button">
                                    <!-- You can hook the "home button" to some JavaScript events or just remove it -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 my-auto">
                    <div class="container-fluid">
                        <div class="row" data-450="opacity:0; left: 130%;position: relative;" data-650="left:0; opacity:1">
                            <div class="col-lg-6">
                                <div class="feature-item">
                                    <i class="icon-screen-smartphone text-primary"></i>
                                    <h3>Device Mockups</h3>
                                    <p class="text-muted">Ready to use HTML/CSS device mockups, no Photoshop required!</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="feature-item">
                                    <i class="icon-camera text-primary"></i>
                                    <h3>Flexible Use</h3>
                                    <p class="text-muted">Put an image, video, animation, or anything else in the screen!</p>
                                </div>
                            </div>
                        </div>
                        <div class="row" data-600="opacity:0; left: 130%;position: relative;" data-750="left:0; opacity:1">
                            <div class="col-lg-6">
                                <div class="feature-item">
                                    <i class="icon-present text-primary"></i>
                                    <h3>Free to Use</h3>
                                    <p class="text-muted">As always, this theme is free to download and use for any purpose!</p>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="feature-item">
                                    <i class="icon-lock-open text-primary"></i>
                                    <h3>Open Source</h3>
                                    <p class="text-muted">Since this theme is MIT licensed, you can use it commercially!</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--Unlimited....--}}
    {{--<section class="features" id="features">--}}
        {{--<div class="container">--}}
            {{--<div class="section-heading text-center">--}}
                {{--<h2>Unlimited Features, Unlimited Fun</h2>--}}
                {{--<p class="text-muted">Check out what you can do with this app theme!</p>--}}
                {{--<hr>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-4 my-auto">--}}
                    {{--<div class="device-container">--}}
                        {{--<div class="device-mockup iphone6 portrait white">--}}
                            {{--<div class="device">--}}
                                {{--<div class="screen">--}}
                                    {{--<!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->--}}
                                    {{--<img src="" class="img-fluid" alt="">--}}
                                {{--</div>--}}
                                {{--<div class="button">--}}
                                    {{--<!-- You can hook the "home button" to some JavaScript events or just remove it -->--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-8 my-auto">--}}
                    {{--<div class="container-fluid">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-lg-6">--}}
                                {{--<div class="feature-item">--}}
                                    {{--<i class="icon-screen-smartphone text-primary"></i>--}}
                                    {{--<h3>Device Mockups</h3>--}}
                                    {{--<p class="text-muted">Ready to use HTML/CSS device mockups, no Photoshop required!</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-6">--}}
                                {{--<div class="feature-item">--}}
                                    {{--<i class="icon-camera text-primary"></i>--}}
                                    {{--<h3>Flexible Use</h3>--}}
                                    {{--<p class="text-muted">Put an image, video, animation, or anything else in the screen!</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-lg-6">--}}
                                {{--<div class="feature-item">--}}
                                    {{--<i class="icon-present text-primary"></i>--}}
                                    {{--<h3>Free to Use</h3>--}}
                                    {{--<p class="text-muted">As always, this theme is free to download and use for any purpose!</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="col-lg-6">--}}
                                {{--<div class="feature-item">--}}
                                    {{--<i class="icon-lock-open text-primary"></i>--}}
                                    {{--<h3>Open Source</h3>--}}
                                    {{--<p class="text-muted">Since this theme is MIT licensed, you can use it commercially!</p>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    {{--Newsletter--}}
    {{--<section class="download bg-primary text-center">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-8 mx-auto">--}}
                    {{--<h2 class="section-heading">Newsletter</h2>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</section>--}}
    {{--Promo--}}
    {{--<section class="cta">--}}
        {{--<div class="cta-content">--}}
            {{--<div class="container">--}}
                {{--<h2>Stop waiting.<br>Start building.</h2>--}}
                {{--<a href="#contact" class="btn btn-outline btn-xl js-scroll-trigger">Let's Get Started!</a>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="overlay"></div>--}}
    {{--</section>--}}
    {{--Contact--}}
    {{--<section class="cta">--}}
        {{--<div class="cta-content">--}}
            {{--<div class="container">--}}
                {{--<h2>Contact</h2>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="overlay"></div>--}}
    {{--</section>--}}
    {{--Download App--}}
    <section class="download bg-primary text-center" id="download">
        <div class="container">
            <div class="row">
                <div class="col-md-8 mx-auto">
                    <h2 class="section-heading" data-950="opacity:0;" data-1000="opacity:1;">Discover what all the buzz is about!</h2>
                    <p>Our app is available on any mobile device! Download now to get started!</p>
                    <div class="badges">
                        <a class="badge-link" href="#"><img src="{{asset('public/images/google-play-badge.svg')}}" alt=""></a>
                        <a class="badge-link" href="#"><img src="{{asset('public/images/app-store-badge.svg')}}" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('frontend.layouts.footer')
@endsection