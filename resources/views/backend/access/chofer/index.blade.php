@extends ('backend.layouts.app')

@section ('title', trans('labels.backend.access.users.management-choferes'))

@section('after-styles')
    {{ Html::style("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.css") }}
@endsection

@section('page-header')
    <h1>
        {{ trans('labels.backend.access.users.management-choferes') }}
        <small>{{ trans('labels.backend.access.cliente.active') }}</small>
    </h1>
@endsection

@section('content')
    {{--@include('backend.access.includes.partials.user-header-buttons')--}}
    <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title">{{ trans('labels.backend.access.choferes.active') }}</h3>

            {{--<div class="box-tools pull-right">--}}
                {{--@include('backend.access.includes.partials.user-header-buttons')--}}
            {{--</div><!--box-tools pull-right-->--}}
        </div><!-- /.box-header -->

        <div class="box-body">


            <table style="text-align: center;" class="table table-striped table-bordered table-hover">
                <thead>
                <tr>
                    <th style="text-align: center;">Nombre</th>
                    <th style="text-align: center;">Apellido</th>
                    <th style="text-align: center;">Correo</th>
                    <th style="text-align: center;">Rol</th>
                    <th style="text-align: center;">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($usuarios as $user)
                    <tr>

                        <td>{!! $user->first_name !!}</td>
                        <td>{!! $user->last_name !!}</td>
                        <td>{!! link_to("mailto:".$user->email, $user->email) !!}</td>
                        <td>
                            @if ($user->roles()->count() > 0)
                                @foreach ($user->roles as $role)
                                    {!! $role->name !!}<br/>
                                @endforeach
                            @else
                                Ninguno
                            @endif
                        </td>
                        <td>{!! $user->action_buttons !!}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{--<div class="table-responsive">--}}
                {{--<table id="users-table" class="table table-condensed table-hover">--}}
                    {{--<thead>--}}
                    {{--<tr>--}}
                        {{--<th>{{ trans('labels.backend.access.users.table.first_name') }}</th>--}}
                        {{--<th>{{ trans('labels.backend.access.users.table.last_name') }}</th>--}}
                        {{--<th>{{ trans('labels.backend.access.users.table.email') }}</th>--}}
                        {{--<th>{{ trans('labels.backend.access.users.table.confirmed') }}</th>--}}
                        {{--<th>{{ trans('labels.backend.access.users.table.roles') }}</th>--}}
                        {{--<th>{{ trans('labels.backend.access.users.table.social') }}</th>--}}
                        {{--<th>{{ trans('labels.backend.access.users.table.created') }}</th>--}}
                        {{--<th>{{ trans('labels.backend.access.users.table.last_updated') }}</th>--}}
                        {{--<th>{{ trans('labels.general.actions') }}</th>--}}
                    {{--</tr>--}}
                    {{--</thead>--}}
                {{--</table>--}}
            {{--</div><!--table-responsive-->--}}
        </div><!-- /.box-body -->
    </div><!--box-->

    {{--<div class="box box-info">--}}
        {{--<div class="box-header with-border">--}}
            {{--<h3 class="box-title">{{ trans('history.backend.recent_history') }}</h3>--}}
            {{--<div class="box-tools pull-right">--}}
                {{--<button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>--}}
            {{--</div><!-- /.box tools -->--}}
        {{--</div><!-- /.box-header -->--}}
        {{--<div class="box-body">--}}
            {{--{!! history()->renderType('User') !!}--}}
        {{--</div><!-- /.box-body -->--}}
    {{--</div><!--box box-success-->--}}
@endsection

@section('after-scripts')
    {{--{{ Html::script("https://cdn.datatables.net/v/bs/dt-1.10.15/datatables.min.js") }}--}}
    {{--<script src="https://datatables.yajrabox.com/js/jquery.min.js"></script>--}}
    <script src="https://datatables.yajrabox.com/js/bootstrap.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>
    <script type="text/javascript">

            {{--$("#cliente").DataTable({--}}
                {{--"processing": true,--}}
                {{--"serverSide": true,--}}
                {{--"ajax": "{{ route('admin.access.cliente.gets') }}",--}}
                {{--"columns": [--}}
                    {{--{data: 'last_name', name: 'last_name'},--}}
{{--//--}}
                {{--]--}}
            {{--});--}}

    </script>
    {{--{{ Html::script("js/backend/plugin/datatables/dataTables-extend.js") }}--}}

    <script>
        $('#cliente').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{route("admin.access.cliente.gets")}}',

            columns: [
//                {data: 'id', name: 'id'},
//                {data: 'name', name: 'name'},
                {data: 'email'},
//                {data: 'created_at', name: 'created_at'},
//                {data: 'updated_at', name: 'updated_at'},
//                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

        {{--$(function () {--}}
            {{--$('#cliente').DataTable({--}}
                {{--"language": {--}}
                    {{--"sProcessing":     "Procesando...",--}}
                    {{--"sLengthMenu":     "Mostrar _MENU_ registros",--}}
                    {{--"sZeroRecords":    "No se encontraron resultados",--}}
                    {{--"sEmptyTable":     "Ningún dato disponible en esta tabla",--}}
                    {{--"sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",--}}
                    {{--"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",--}}
                    {{--"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",--}}
                    {{--"sInfoPostFix":    "",--}}
                    {{--"sSearch":         "Buscar:",--}}
                    {{--"searchPlaceholder": "Clientes",--}}
                    {{--"sUrl":            "",--}}
                    {{--"sInfoThousands":  ",",--}}
                    {{--"sLoadingRecords": "Cargando...",--}}
                    {{--"oPaginate": {--}}
                        {{--"sFirst":    "Primero",--}}
                        {{--"sLast":     "Último",--}}
                        {{--"sNext":     "Siguiente",--}}
                        {{--"sPrevious": "Anterior"--}}
                    {{--},--}}
                    {{--"oAria": {--}}
                        {{--"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",--}}
                        {{--"sSortDescending": ": Activar para ordenar la columna de manera descendente"--}}
                    {{--}--}}
                {{--},--}}
{{--//                dom: 'lfrtip',--}}
                {{--processing: true,--}}
                {{--serverSide: true,--}}
                {{--autoWidth: false,--}}
                {{--ajax: {--}}
                    {{--url: '{{ route("admin.access.cliente.gets") }}',--}}
                    {{--type: 'post',--}}
                    {{--data: {status: 1, trashed: false},--}}
                    {{--error: function (xhr, err) {--}}
                        {{--if (err === 'parsererror')--}}
                            {{--location.reload();--}}
                    {{--}--}}
                {{--},--}}
                {{--columns: [--}}
                    {{--{data: 'first_name', name: 'first_name'},--}}
{{--//                    {data: 'last_name', name: 'last_name'},--}}
{{--//                    {data: 'email', name: 'email'},--}}
{{--//                    {data: 'confirmed', name: 'confirmed'},--}}
{{--//                    {data: 'roles', name: 'name', sortable: false},--}}
{{--////                    {data: 'social', name: 'social', sortable: false},--}}
{{--//                    {data: 'created_at', name: 'created_at'},--}}
{{--//                    {data: 'updated_at', name: 'updated_at'},--}}
{{--//                    {data: 'actions', name: 'actions', searchable: false, sortable: false}--}}
                {{--],--}}
                {{--order: [[0, "asc"]],--}}
                {{--searchDelay: 500--}}
            {{--});--}}
        {{--});--}}
    {{--</script>--}}
@endsection
