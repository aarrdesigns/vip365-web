<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ access()->user()->picture }}" class="img-circle" alt="User Image" />
            </div><!--pull-left-->
            <div class="pull-left info">
                <p>{{ access()->user()->name }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('strings.backend.general.status.online') }}</a>
            </div><!--pull-left-->
        </div><!--user-panel-->

        <!-- search form (Optional) -->
        {{--{{ Form::open(['route' => 'admin.search.index', 'method' => 'get', 'class' => 'sidebar-form']) }}--}}
        {{--<div class="input-group">--}}
            {{--{{ Form::text('q', Request::get('q'), ['class' => 'form-control', 'required' => 'required', 'placeholder' => trans('strings.backend.general.search_placeholder')]) }}--}}

            {{--<span class="input-group-btn">--}}
                    {{--<button type='submit' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>--}}
                  {{--</span><!--input-group-btn-->--}}
        {{--</div><!--input-group-->--}}
    {{--{{ Form::close() }}--}}
    <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('menus.backend.sidebar.general') }}</li>

            <li class="{{ active_class(Active::checkUriPattern('admin/dashboard')) }}">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="fa fa-dashboard"></i>
                    <span>{{ trans('menus.backend.sidebar.dashboard') }}</span>
                </a>
            </li>

            <li class="header">{{ trans('menus.backend.sidebar.system') }}</li>

            @role(1)
            <!-- usuarios -->
            <li class="{{ active_class(Active::checkUriPattern('admin/access/user*')) }}">
                <a href="{{ route('admin.access.user.index') }}">
                    <i class="fa fa-users"></i>
                    <span>{{ trans('menus.backend.access.title-users') }}</span>
                </a>

            </li>
            <!-- Clientes -->
            <li class="{{ active_class(Active::checkUriPattern('admin/access/cliente*')) }}">
                <a href="{{ route('admin.access.cliente.index') }}">
                    <i class="fa fa-child"></i>
                    <span>{{ trans('menus.backend.access.title-clients') }}</span>
                </a>
            </li>
            <!-- Choferes -->
            <li class="{{ active_class(Active::checkUriPattern('admin/access/chofer*')) }}">
                <a href="{{ route('admin.access.chofer.index') }}">
                    <i class="fa fa-car"></i>
                    <span>{{ trans('menus.backend.access.title-choferes') }}</span>
                </a>
            </li>
            {{--<!-- trips -->--}}
            <li class="{{ active_class(Active::checkUriPattern('admin/access/trips')) }}">
                <a href="{{ route('admin.access.trips.index') }}">
                    <i class="fa fa-suitcase"></i>
                    <span>{{ trans('menus.backend.access.title-trips') }}</span>
                </a>
            </li>
            {{--<!-- Finanzas -->--}}
            {{--<li class="{{ active_class(Active::checkUriPattern('admin/access/*')) }} treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-balance-scale"></i>--}}
                    {{--<span>{{ trans('menus.backend.access.title-finanzas') }}</span>--}}

                    {{--@if ($pending_approval > 0)--}}
                        {{--<span class="label label-danger pull-right">{{ $pending_approval }}</span>--}}
                    {{--@else--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--@endif--}}
                {{--</a>--}}

                {{--<ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/access/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/access/*'), 'display: block;') }}">--}}
                    {{--<li class="{{ active_class(Active::checkUriPattern('admin/access/user*')) }}">--}}
                        {{--<a href="{{ route('admin.access.finanzas.index') }}">--}}
                            {{--<i class="fa fa-circle-o"></i>--}}
                            {{--<span>{{ trans('labels.backend.access.users.management-finanzas') }}</span>--}}

                            {{--@if ($pending_approval > 0)--}}
                                {{--<span class="label label-danger pull-right">{{ $pending_approval }}</span>--}}
                            {{--@endif--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            {{--<!-- charts -->--}}
            {{--<li class="{{ active_class(Active::checkUriPattern('admin/access/*')) }} treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-area-chart"></i>--}}
                    {{--<span>{{ trans('menus.backend.access.title-charts') }}</span>--}}

                    {{--@if ($pending_approval > 0)--}}
                        {{--<span class="label label-danger pull-right">{{ $pending_approval }}</span>--}}
                    {{--@else--}}
                        {{--<i class="fa fa-angle-left pull-right"></i>--}}
                    {{--@endif--}}
                {{--</a>--}}

                {{--<ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/access/*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/access/*'), 'display: block;') }}">--}}
                    {{--<li class="{{ active_class(Active::checkUriPattern('admin/access/user*')) }}">--}}
                        {{--<a href="{{ route('admin.access.charts.index') }}">--}}
                            {{--<i class="fa fa-circle-o"></i>--}}
                            {{--<span>{{ trans('labels.backend.access.users.management-charts') }}</span>--}}

                            {{--@if ($pending_approval > 0)--}}
                                {{--<span class="label label-danger pull-right">{{ $pending_approval }}</span>--}}
                            {{--@endif--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
            @endauth

            {{--<li class="{{ active_class(Active::checkUriPattern('admin/log-viewer*')) }} treeview">--}}
                {{--<a href="#">--}}
                    {{--<i class="fa fa-list"></i>--}}
                    {{--<span>{{ trans('menus.backend.log-viewer.main') }}</span>--}}
                    {{--<i class="fa fa-angle-left pull-right"></i>--}}
                {{--</a>--}}
                {{--<ul class="treeview-menu {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'menu-open') }}" style="display: none; {{ active_class(Active::checkUriPattern('admin/log-viewer*'), 'display: block;') }}">--}}
                    {{--<li class="{{ active_class(Active::checkUriPattern('admin/log-viewer')) }}">--}}
                        {{--<a href="{{ route('log-viewer::dashboard') }}">--}}
                            {{--<i class="fa fa-circle-o"></i>--}}
                            {{--<span>{{ trans('menus.backend.log-viewer.dashboard') }}</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}

                    {{--<li class="{{ active_class(Active::checkUriPattern('admin/log-viewer/logs')) }}">--}}
                        {{--<a href="{{ route('log-viewer::logs.list') }}">--}}
                            {{--<i class="fa fa-circle-o"></i>--}}
                            {{--<span>{{ trans('menus.backend.log-viewer.logs') }}</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        </ul><!-- /.sidebar-menu -->
    </section><!-- /.sidebar -->
</aside>